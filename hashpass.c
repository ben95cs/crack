#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[]) {

	FILE * rock = fopen("rockyou100.txt", "r");
	FILE * hashes = fopen("hashes.txt", "w");
	char pass[15];
	
	if (rock) {
		while (fgets(pass, 15, rock) != NULL) {
			
			// Store line as a string, omitting the last character being the null character at the end
			char * hash = md5(pass, strlen(pass) - 1);
			
			// Write the result to hashes.txt
			fprintf(hashes, "%s\n", hash);
			
			// Free the memory from calling the md5 function
			free(hash);
		}
		fclose(rock);
		fclose(hashes);
	}
	else {
		printf("ERROR: Could not read file\n");
		exit(1);
	}
}
